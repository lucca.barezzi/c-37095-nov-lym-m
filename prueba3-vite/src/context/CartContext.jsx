import { useState, createContext, useContext } from "react";

const CartContext = createContext([])

export const useCartContext = () => useContext(CartContext) 


export const CartContextProvider = ({ children }) => {
    const [ cartList, setCartList ] = useState([])
    // esados y funciones globales

    // { camposProductos, cantidad }
    const agregarCarrito = (producto) => {
        setCartList( [
            ...cartList,
            producto
        ] )
        // setear en localstorage
    }

    // vaciar carrtio

    const vaciarCarrito = () => setCartList([])

    // precioTotal
    // cantidadTotal

    return (
        <CartContext.Provider value={{
            cartList,
            agregarCarrito,
            vaciarCarrito
        }}>
            { children }
        </CartContext.Provider>
    )
}

// casos de usos: 
// carrito compra contexto 
    // guardar usario logueado 
    // darkmode