
import Item from "../Item/Item"

const ItemList = ({ products }) => {

  // const { personas } = useContext(ContextApp)

  // console.log(obj)

  return (

    <div style={{
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap'
    }}>
      { products.map( product =>    <Item product={product} />  )}

    </div>
    
   
  )
}

export default ItemList