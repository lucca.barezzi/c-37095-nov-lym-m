import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)


  let condition = true

  // let resultado = ''
  // if (condition) {
  //   resultado = 'Verdadero'
  // } else {
  //   resultado = 'Falso'
  // }

  // if else -> condición ? true : false - condition && 'Verdadero' - condition || 'Falso'

  // console.log('El resultado es: ' + resultado);

  // console.log(`El resultado es: ${ condition ? 'Verdadero' : 'Falso' }`);

  // i = i + 1 (contador) => i += 1 => i++

  // Spreed operator 
  // let uno = 1
  // let arrayNum = [ 2,3,4 ]
  // // let [1,2,3,4]

  // let newArray = [ ...arrayNum, uno ] 
  // console.log( newArray)

  //propiedades dinámicas

//   let campo = 'id'

//   const obj = {
//     name: 'Juan',
//     apellido: 'Perez',
//     [ campo + '_edad']: 25
//   }
// console.log(obj)

// deep maching
// const obj = {
//       name: 'Juan',
//       apellido: 'Perez' ,
//       edad: 35    
//     }

    // const name = obj.name
    // const apellido = obj.apellido

    //destructuración 
    // const { name = name } = obj

    // const { name: firstName, apellido: lastName, edad = 25  } = obj

    // console.log(edad)

  //    numero 3 si existe  <- [1,2,3,4].find((num)=> num === 3)

  // pollyfill -> librería que simula una funcionalidad que no existe en el navegador
  // const find = (array, callback) => {
  //   for (let i = 0; i < array.length; i++) {
  //     if (callback(array[i])) {
  //       return array[i]
  //     }
  //   }
  // }  

  // console.log(find([1,2,3,4], (num) => num === 5))





  // for (const num of arr) {
  //   if (num==3){
  //       console.log(num);
  //   }    
  
//   let arr = [1,2,3,4,5,6]

//   function findInArr(arr, find){
//       for(let i = 0; i < arr.length; i++){
//           if(arr[i] === find){
//               return arr[i];
//           };
//       };
//   };
  
// console.log(findInArr(arr, 3))
  


  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Fede el mejor</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App



  

