import {useState, useEffect} from 'react'
// import './input.css'


export const Input = () => {   

    
    
    // useEffect(()=>{
    //     window.addEventListener('click', inputHandler)
    //     // misión es no usar esto
    //     return () => {
            
    //         window.removeEventListener('click', inputHandler)
    //     }
    // })

    const inputHandler = (event)=>{
        // event.stopPropagation()
        if (['a','e','i', 'o', 'u'].includes(event.key)) {
            event.preventDefault()            
        }
        console.log(event.key)
    //    console.log(event.target.name)
    //    console.log(event.target.value)

    }

    return (
        <div className="box" >
            <div 
                className="border border-5 border-warning" 
            >
                <input 
                    className="m-5" 
                    onKeyDown={ inputHandler } 
                    // onClick={  inputHandler } 
                    type="text" 
                    name="nombre" 
                    id="i"
                />
                
            </div>
        </div>
    )
}
